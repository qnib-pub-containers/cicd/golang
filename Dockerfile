FROM golang:1.23.1-bullseye

RUN apt update \
 && apt-get install -y pkg-config libgpgme-dev libdevmapper-dev \
 && go install gitlab.com/fgmarand/gocoverstats@latest \
 && go install github.com/t-yuki/gocover-cobertura@latest
